package pc.examples

import pc.utils.Time
import pc.modelling.CTMCSimulation
import java.util.Random

object StochasticChannelSimulation extends App {

  import pc.examples.StochasticChannel.state._

  val channel = StochasticChannel.stocChannel

  val channelAnalysis = CTMCSimulation(channel)
  Time.timed{
    println(channelAnalysis.newSimulationTrace(IDLE, new Random)
                           .take(100)
                           .toList
                           .mkString("\n"))
  }

  println("Average time to reach DONE state: " + channelAnalysis.reachStateAverageTime(IDLE, DONE, new Random, 10000))
  println("Average percent time in FAIL state: " + channelAnalysis.relativeFailTime(IDLE, DONE, FAIL, new Random, 10000))

}