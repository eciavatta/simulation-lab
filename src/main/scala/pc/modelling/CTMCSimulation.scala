package pc.modelling

import java.util.Random

import pc.utils.Stochastics

trait CTMCSimulation[S] { self: CTMC[S] =>

  private val DefaultRuns: Int = 100
  private val NoneDouble: Option[Double] = None

  type Trace[A] = Stream[(Double,A)]

  def newSimulationTrace(s0: S, rnd: Random): Trace[S] =
    Stream.iterate( (0.0,s0) ){ case (t,s) => {
      if (transitions(s).isEmpty) (t,s) else {
        val next = Stochastics.cumulative(transitions(s).toList)
        val sumR = next.last._1
        val choice = Stochastics.draw(next)(rnd)
        (t + Math.log(1 / rnd.nextDouble()) / sumR, choice)
      }
    }}

  def reachStateAverageTime(s0: S, dst: S, rnd: Random, runs: Int = DefaultRuns): Double = (0 to runs).map { _ =>
    newSimulationTrace(s0, rnd).collectFirst { case (t, `dst`) => t }.get
  }.sum / runs

  def relativeFailTime(s0: S, dst: S, failState: S, rnd: Random, runs: Int = DefaultRuns): Double = (0 to runs).map { _ =>
    val trace = newSimulationTrace(s0, rnd).takeWhile(_._2 != dst)
    val failTime = trace.foldLeft((0.0, NoneDouble)) {
      case ((totalFail, None), (current, `failState`)) => (totalFail, Some(current))
      case ((totalFail, Some(old)), (_, `failState`)) => (totalFail, Some(old))
      case ((totalFail, None), _) => (totalFail, None)
      case ((totalFail, Some(old)), (current, _)) => (totalFail + current - old, None)
    }._1
    failTime / trace.last._1
  }.sum / runs

}

object CTMCSimulation {
  def apply[S](ctmc: CTMC[S]): CTMCSimulation[S] =
    new CTMC[S] with CTMCSimulation[S]{
      override def transitions(s: S) = ctmc.transitions(s)
    }
}

object Test extends App {
  val l = Stream.iterate( (0.0,"ciao")) {
    case (t, s) => (t + 1, "ciao")
  }.take(5).toList

  println(l)



}

